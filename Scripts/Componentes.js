$(window).load(function () {
    $("#select").attr('disabled', 'disabled');
    $("#BtnAgregarSentencias").attr('disabled', 'disabled');
});

var sentencias = false;
var cantidadsentencias = 0;
var cantidadcasos = 0;
var respuesta = "";


function Agregar() {
    if (cantidadcasos < parseInt($("#numerodecasos").val())) {
        var numeroCasos = parseInt($("#numerodecasos").val());
        var dimensionMatriz = parseInt($("#dimensionmatriz").val());
        var numeroSentencias = parseInt($("#numerosentencias").val());
        if (numeroCasos >= 1 && numeroCasos <= 50) {
            if (dimensionMatriz >= 1 && dimensionMatriz <= 100) {
                if (numeroSentencias >= 1 && numeroSentencias <= 1000) {
                    $("#select").removeAttr('disabled', 'disabled');
                    $("#BtnAgregarSentencias").removeAttr('disabled', 'disabled');

                    $("#numerodecasos").attr('disabled', 'disabled');
                    $("#numerosentencias").attr('disabled', 'disabled');
                    $("#dimensionmatriz").attr('disabled', 'disabled');
                    if (cantidadcasos === 0)
                        $("#textArea").val($("#textArea").val() + "\n" + numeroCasos);
                    cantidadcasos++;

                    $("#textArea").val($("#textArea").val() + "\n" + dimensionMatriz + " " + numeroSentencias);
                    $("#sentencias").html('<div class="form-group col-lg-1" ><label for="x">X</label><input type="text" class="form-control" id="x" placeholder="x"> </div> <div class="form-group col-lg-1" ><label for="y">Y</label><input type="text" class="form-control" id="y" placeholder="Y"> </div>  <div class="form-group col-lg-1" ><label for="z">Z</label><input type="text" class="form-control" id="z" placeholder="Z"> </div><div class="form-group col-lg-1" ><label for="w">W</label><input type="text" class="form-control" id="w" placeholder="W"> </div>');
                    sentencias = true;
                    Mensaje("exito", "Configure el primer caso de prueba");
                } else {
                    Mensaje("error", "El numero de sentencias debe ser mayor o igual que 1 y menor o igual  que 1000");
                }
            } else {
                Mensaje("error", "La dimension de la matriz debe ser mayor o igual  que 1 y menor o igual que 100");
            }
        } else {
            Mensaje("error", "El numero de casos de test debe ser mayor o igual que 1 y menor o igual que 50");
        }
    }
}

function select() {
    if (sentencias === true) {
        var valor = $("#select").val();
        if (valor === "Actualización") {
            $("#sentencias").html('<div class="form-group col-lg-1" ><label for="x">X</label><input type="text" class="form-control" id="x" placeholder="x"> </div> <div class="form-group col-lg-1" ><label for="y">Y</label><input type="text" class="form-control" id="y" placeholder="Y"> </div>  <div class="form-group col-lg-1" ><label for="z">Z</label><input type="text" class="form-control" id="z" placeholder="Z"> </div><div class="form-group col-lg-1" ><label for="w">W</label><input type="text" class="form-control" id="w" placeholder="W"> </div>');
        }
        else {
            $("#sentencias").html('<div class="form-group col-lg-1" ><label for="x1">X1</label><input type="text" class="form-control" id="x1" placeholder="x1"> </div> <div class="form-group col-lg-1" ><label for="y1">Y1</label><input type="text" class="form-control" id="y1" placeholder="Y1"> </div>  <div class="form-group col-lg-1" ><label for="z1">Z1</label><input type="text" class="form-control" id="z1" placeholder="Z1"> </div> <div class="form-group col-lg-1" ><label for="x2">X2</label><input type="text" class="form-control" id="x2" placeholder="x2"> </div> <div class="form-group col-lg-1" ><label for="y2">Y2</label><input type="text" class="form-control" id="y2" placeholder="Y2"> </div>  <div class="form-group col-lg-1" ><label for="z2">Z2</label><input type="text" class="form-control" id="z2" placeholder="Z2">');
        }
    }

}
function AgregarSentencias() {
    var cor = "";
    if (sentencias === true) {

        var valor = $("#select").val();
        if (valor === "Actualización") {
            var x = parseInt($("#x").val());
            var y = parseInt($("#y").val());
            var z = parseInt($("#z").val());
            var w = parseInt($("#w").val());
            if (x >= 1 && x <= parseInt($("#dimensionmatriz").val()) && y >= 1 && y <= parseInt($("#dimensionmatriz").val()) && z >= 1 && z <= parseInt($("#dimensionmatriz").val())) {
                $("#textArea").val($("#textArea").val() + "\n" + valor + " " + x + " " + y + " " + z + " " + w);
                cor = x + "-" + y + "-" + z + "-" + w;
                if (cantidadsentencias === 0) {
                    CalcularMatriz("crear", cor);
                } else {
                    CalcularMatriz("calcular", cor);
                }
            } else {
                Mensaje("error", "x,y,z deben se mayor o igual a 1 y menores que la dimension de la matriz");

            }
        } else {
            var x1 = parseInt($("#x1").val());
            var y1 = parseInt($("#y1").val());
            var z1 = parseInt($("#z1").val());
            var x2 = parseInt($("#x2").val());
            var y2 = parseInt($("#y2").val());
            var z2 = parseInt($("#z2").val());
            if (x1 >= 1 && x1 <= parseInt($("#dimensionmatriz").val()) && y1 >= 1 && y1 <= parseInt($("#dimensionmatriz").val()) && z1 >= 1 && z1 <= parseInt($("#dimensionmatriz").val())) {
                if (x2 >= 1 && x2 <= parseInt($("#dimensionmatriz").val()) && y2 >= 1 && y2 <= parseInt($("#dimensionmatriz").val()) && z2 >= 1 && z2 <= parseInt($("#dimensionmatriz").val())) {
                    $("#textArea").val($("#textArea").val() + "\n" + valor + " " + x1 + " " + y1 + " " + z1 + " " + x2 + " " + y2 + " " + z2);
                    cor = x1 + "-" + y1 + "-" + z1 + "-" + x2 + "-" + y2 + "-" + z2;
                    if (cantidadsentencias === 0) {
                        CalcularMatriz("crear", cor);
                    } else {
                        CalcularMatriz("calcular", cor);
                    }
                }
                else {
                    Mensaje("error", "x2,y2,z2 deben se mayor o igual a 1 y menores que la dimension de la matriz");
                }
            } else {
                Mensaje("error", "x1,y1,z1 deben se mayor o igual a 1 y menores que la dimension de la matriz");

            }

        }
        cantidadsentencias++;

        if (cantidadsentencias === parseInt($("#numerosentencias").val())) {
            if (cantidadcasos === parseInt($("#numerodecasos").val())) {
                Mensaje("exito", "ha terminado la configuración");
                sentencias = false;
                ActivarModal();
            } else {
                $("#dimensionmatriz").removeAttr('disabled', 'disabled');
                $("#numerosentencias").removeAttr('disabled', 'disabled');
                $("#select").attr('disabled', 'disabled');
                $("#BtnAgregarSentencias").attr('disabled', 'disabled');
                $("#sentencias").attr('disabled', 'disabled');

                sentencias = false;
                cantidadsentencias = 0;
                Mensaje("exito", "configure el siguiente caso de prueba");
            }
        }
    }
}


function Mensaje(tipo, mensaje) {
    if (tipo === "error") {
        $("#alertas").html("<div class='alert alert-danger' role='alert'><p>" + mensaje + "</p></div>");
    } else {
        $("#alertas").html("<div class='alert alert-success' role='alert'><p>" + mensaje + "</p></div>");
    }
}

function CalcularMatriz(accion, cor) {

    var datos = {accion: accion, dimensionmatriz: $("#dimensionmatriz").val(), sentencia: $("#select").val(), cordenadas: cor};
    $.ajax({
        method: "POST",
        url: "Controlador/CtlMatriz.php",
        data: datos,
        success: ProcesarCalcularmatriz,
        error: function () {
            Mensaje("error", "Error listando los inmuebles, intentelo de nuevo");
        }
    });
}

var ProcesarCalcularmatriz = function (data) {
    alert("llegandoooooo a procesar  "+respuesta);
    respuesta = respuesta + " " + data.split("+")[0];
    alert("llegandoooooo a procesar2222  "+respuesta);

};

function ActivarModal() {
    $("#myModal").modal("show");
    $("#repuesta").html("<p>" + respuesta + "</p>");
}
function Recargarpg() {
    alert("recargado")
    window.location.reload();
}
